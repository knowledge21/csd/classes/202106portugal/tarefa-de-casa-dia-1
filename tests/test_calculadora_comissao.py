from unittest import TestCase
import sistema_vendas.calculadora_comissao as calculadora_comissao

# Comissão = 5% se venda < 10K
# Comissão = 6% se venda >= 10K

# Lula
#Ju
# Nuno

#CÓDIGO DE TESTE!
class TestCalculadoraComissao(TestCase):

    def teste_soma_1_e_2_e_retorna_3_como_resultado(self):

        #ARRANGE
        umValorQueEuDigitei = 1
        outroValorQueEuDigitei = 2
        resultadoEsperado = 3

        #ACT
        resultadoCalculado = calculadora_comissao.somaesserole(umValorQueEuDigitei, outroValorQueEuDigitei)

        #ASSERT
        self.assertEqual(resultadoCalculado, resultadoEsperado)

    def teste_comissao_venda_de_500_retorna_25_como_resultado(self):

        # ARRANGE
        valorVenda = 500
        resultadoEsperado = 25

        # ACT
        resultadoCalculado = calculadora_comissao.calculacomissao(valorVenda)

        # ASSERT
        self.assertEqual(resultadoCalculado, resultadoEsperado)